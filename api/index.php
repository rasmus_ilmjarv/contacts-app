<?php
require_once("ContactDao.php");
require_once("Contact.php");
require_once("functions.php");

$contactDao = new ContactDao();

$cmd = "findAll";
if (isset($_GET["cmd"])) {
    $cmd = $_GET["cmd"];
}

if ($cmd === "findAll") {

    $contacts = $contactDao->findAll();
    printJson($contacts);

} else if ($cmd === "findById") {

    $id = intval($_GET["id"]);
    $contact = $contactDao->findById($id);

    if ($contact == null) {
        http_response_code(404);
    } else {
        printJson($contact);
    }

} else if ($cmd === "add") {

    $json = file_get_contents("php://input");
    $contactData = json_decode($json, true);

    $contact = Contact::fromAssocArray($contactData);
    $errors = $contact->validate();

    if (count($errors) > 0) {
        http_response_code(400);
        printJson(["errors" => $errors]);
    } else {
        $contactDao->save($contact);
        printJson($contact);
    }

} else if ($cmd === "edit") {

    $json = file_get_contents("php://input");
    $contactData = json_decode($json, true);
    $id = $_GET["id"];

    $contact = Contact::fromAssocArray($contactData);
    $contact->id = $id;
    $errors = $contact->validate();

    if (count($errors) > 0) {
        http_response_code(400);
        printJson(["errors" => $errors]);
    } else {
        $contactDao->update($contact);
        printJson($contact);
    }

} else if ($cmd === "delete") {

    $id = intval($_GET["id"]);
    $contactDao->deleteById($id);
    http_response_code(204);

} else {
    http_response_code(400);
    printJson(["error" => "Unknown command: ${cmd}"]);
}