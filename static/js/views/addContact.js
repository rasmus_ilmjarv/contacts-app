import { elem, formGroup, displayErrors} from "../elements.js";
import { navigateTo } from "../router.js";

const addContact = async () => {
    document.title = "Lisa kontakt";

    const page = elem("div");
    page.id = "page-container";
    const title = elem("h1", "Add Contact");
    const form = createContactAddForm();

    page.appendChild(title);
    page.appendChild(form);
    return page;
}

function createContactAddForm() {
    const nameGroup = formGroup("Kontakti nimi", "nameInput");
    const emailGroup = formGroup("E-mail", "emailInput");
    const phoneGroup = formGroup("Telefoninumber", "phoneInput");

    const submitButton = elem("button", "Salvesta");
    submitButton.className = "btn btn-primary";
    submitButton.type = "button";
    submitButton.addEventListener("click", e => {
        const name = document.getElementById("nameInput").value;
        const email = document.getElementById("emailInput").value;
        const phone = document.getElementById("phoneInput").value;

        const update = {
            name,
            email,
            phone
        };

        fetch(`/api/index.php?cmd=add`, {
            method: "POST",
            body: JSON.stringify(update)
        })
            .then(async response => {
                if (response.ok) {
                    return response.json();
                }
                return response.json().then(errorBody => {
                    const error = new Error(response.statusText);
                    error.errorBody = errorBody;
                    throw error;
                });
            })
            .then(data => {
                navigateTo("/?success=true", );
            })
            .catch(error => displayErrors(error.errorBody.errors));

    });

    return elem("form",
        nameGroup,
        emailGroup,
        phoneGroup,
        submitButton);
}

export default addContact;