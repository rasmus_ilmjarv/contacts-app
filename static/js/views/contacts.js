import { elem } from "../elements.js";

const contacts = async (params) => {
    document.title = "Contacts";

    const contacts = await fetch("/api/index.php?cmd=findAll")
        .then(response => response.json());

    const page = document.createElement("div");
    const title = document.createElement("h1");
    title.textContent = "Contacts";
    page.appendChild(title);

    const tbody = elem("tbody");
    contacts.forEach(contact => {
        const row = elem("tr",
            elem("td", contact.id),
            elem("td", linkToContact(contact)),
            elem("td", contact.email),
            elem("td", contact.phone),
            elem("td", deleteContactButton(contact))
        );

        tbody.appendChild(row);
    });

    const table = elem("table",
        elem("thead",
            elem("tr",
                elem("th", "ID"),
                elem("th", "Nimi"),
                elem("th", "E-mail"),
                elem("th", "Telefoninumber"),
                elem("th")
            )
        ),
        tbody
    )
    table.className = "table";

    if (params.get('success') && params.get('success') === "true") {
        const alert = elem("div", "Salvestamine õnnestus");
        alert.className = "alert alert-success";
        alert.role = "alert";

        page.appendChild(alert);
    }

    page.appendChild(table);
    return page;
}

function deleteContactButton(contact) {
    const button = elem("button", "Kustuta");
    button.className = "btn btn-danger";
    button.type = "button";

    button.addEventListener("click", e => {
        fetch(`/api/index.php?cmd=delete&id=${contact.id}`, { method: "POST" })
            .then(response => {
                if (response.ok) {
                    e.target.closest("tr").remove();
                }
            });
    });


    return button;
}

function linkToContact(contact) {
    const link = elem("a", contact.name);
    link.href = `/contact?id=${encodeURIComponent(contact.id)}`;
    link.setAttribute("data-link", "");
    return link;
}

export default contacts;