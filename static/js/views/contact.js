import { navigateTo } from "../router.js";
import { elem, formGroup, displayErrors } from "../elements.js";

const contact = async (params) => {
    document.title = "Contact";
    const page = elem("div");
    page.id = "page-container";

    const contactId = params.get("id");
    let contact;
    if (contactId) {
        contact = await fetch(`/api/index.php?cmd=findById&id=${encodeURIComponent(contactId)}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.status);
                }

                return response;
            })
            .then(response => response.json())
            .catch(error => console.log("Contact not found"));
    }

    if (contact) {
        const contactEditForm = createContactEditForm(contact);
        page.appendChild(contactEditForm);
    } else {
        const alert = elem("div", "Kontakti ei leitud");
        alert.className = "alert alert-danger";
        alert.role = "alert";
        page.appendChild(alert);
    }

    return page;
}

function createContactEditForm(contact) {
    const nameGroup = formGroup("Kontakti nimi", "nameInput", contact.name);
    const emailGroup = formGroup("E-mail", "emailInput", contact.email);
    const phoneGroup = formGroup("Telefoninumber", "phoneInput", contact.phone);

    const submitButton = elem("button", "Salvesta");
    submitButton.className = "btn btn-primary";
    submitButton.type = "button";
    submitButton.addEventListener("click", e => {
        const name = document.getElementById("nameInput").value;
        const email = document.getElementById("emailInput").value;
        const phone = document.getElementById("phoneInput").value;

        const update = {
            name,
            email,
            phone
        };

        fetch(`/api/index.php?cmd=edit&id=${encodeURIComponent(contact.id)}`, {
            method: "POST",
            body: JSON.stringify(update)
            })
            .then(async response => {
                if (response.ok) {
                    return response.json();
                }
                return response.json().then(errorBody => {
                    const error = new Error(response.statusText);
                    error.errorBody = errorBody;
                    throw error;
                });
            })
            .then(data => {
                navigateTo("/?success=true", );
            })
            .catch(error => displayErrors(error.errorBody.errors));

    });

    return elem("form",
        nameGroup,
        emailGroup,
        phoneGroup,
        submitButton);
}

export default contact;