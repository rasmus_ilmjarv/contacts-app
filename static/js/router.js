import contacts from "./views/contacts.js";
import addContact from "./views/addContact.js";
import contact from "./views/contact.js";

export const navigateTo = (path) => {
    history.pushState(null, null, path);
    router();
}

const removeAllChildNodes = (element) => {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

export const router = async () => {
    const routes = [
        { path: "/", renderView: contacts},
        { path: "/add-contact", renderView: addContact },
        { path: "/contact", renderView: contact }
    ];
    const defaultRoute = routes[0];
    const currentPath = location.pathname;
    const params = new URLSearchParams(location.search);

    const routeMatch = routes.find(route => route.path === currentPath);

    const app = document.getElementById("app");
    removeAllChildNodes(app);
    if (routeMatch) {
        const view = await routeMatch.renderView(params)
        app.appendChild(view);
    } else {
        const view = await defaultRoute.renderView(params);
        app.appendChild(view);
    }
}